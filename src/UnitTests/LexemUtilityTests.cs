﻿using System;
using System.Linq;
using NUnit.Framework;
using TestSystem.CandidatSolve.SplitLexems;

namespace UnitTests
{
    /// <summary>
    /// Тесты на 
    /// </summary>
    [TestFixture]
    public class LexemUtilityTests
    {
        private const string _SMALL_VALUE = "aBc";
        private const string _MEDIUM_NOT_EVEN_VALUE = "aB@sdf.wer45kwerKJWERWWER";
        private const string _MEDIUM_EVEN_VALUE = "[1LDNj,e25nw,sj3504k";

        private const string _LARGE_VALUE = "SDF25343123asdfwsf2e@seflmwer.,wer[posdfqlkwjepoqwe3213qwe24126354123123qADSsa";

        /// <summary>
        /// Тестируемый метод: FindAllLexems
        /// Сценарий: Позитивный сценарий, передаем валидные аргументы
        /// Ожидаемый результат: Метод корректно отработал и вернул правильные данные
        /// </summary>
        /// <param name="value">Исходное значение</param>
        /// <param name="lexemLength">Длина лексем</param>
        /// <param name="lexems">Какие на выходы должны получить лексемы</param>
        [TestCase("a", 1, "a")]
        [TestCase("", 1)]
        [TestCase(_SMALL_VALUE, 1, "a", "B", "c")]
        [TestCase(_SMALL_VALUE, 2, "aB", "Bc")]
        [TestCase(_SMALL_VALUE, 3, _SMALL_VALUE)]
        [TestCase(_SMALL_VALUE, 4)]
        [TestCase(_MEDIUM_NOT_EVEN_VALUE, 20, "aB@sdf.wer45kwerKJWE", "B@sdf.wer45kwerKJWER", "@sdf.wer45kwerKJWERW",
            "sdf.wer45kwerKJWERWW", "df.wer45kwerKJWERWWE", "f.wer45kwerKJWERWWER")]
        public void FindAllLexems_ValidCall_ValidResult(string value, int lexemLength, params string[] lexems)
        {
            var utility = new LexemUtility();
            var result = utility.FindAllLexems(value, lexemLength).ToArray();
            Assert.AreEqual(lexems.Length, result.Length);

            for (int i = 0; i < result.Length; i++)
            {
                Assert.AreEqual(lexems[i], result[i]);
            }
        }

        /// <summary>
        /// Тестируемый метод: SplitToLexems
        /// Сценарий: передаем массив с некорректными длинами лексем
        /// Ожидаемый результат: Метод не вернул лексем
        /// </summary>
        [TestCase(_SMALL_VALUE, new int[] { })]
        [TestCase(_SMALL_VALUE, new int[] { 0 })]
        [TestCase(_SMALL_VALUE, new int[] { -1 })]
        [TestCase(_SMALL_VALUE, new int[] { -1, -2 })]
        public void SplitToLexems_SendInvalidLengths_NoLexems(string value, int[] lengths)
        {
            TestSplitToLexems(value, lengths);
        }

        /// <summary>
        /// Тестируемый метод: SplitToLexems
        /// Сценарий: передаем одну длину лексем, которая совпадает с длиной самой строки
        /// Ожидаемый результат: Метод вернул саму разбиваемую строку
        /// </summary>
        [TestCase("1")]
        [TestCase(_SMALL_VALUE)]
        [TestCase(_MEDIUM_NOT_EVEN_VALUE)]
        [TestCase(_LARGE_VALUE)]
        public void SplitToLexems_FullLength_OriginalValue(string value)
        {
            TestSplitToLexems(value, new[] {value.Length}, value);
        }

        /// <summary>
        /// Тестируемый метод: SplitToLexems
        /// Сценарий: Передаем одну длину лексем, которая меньше длины строки
        /// Ожидаемый результат: Метод вернул правильные лексемы
        /// </summary>
        [TestCase(_SMALL_VALUE, 2, "aB")]
        [TestCase(_MEDIUM_NOT_EVEN_VALUE, 4, "aB@s", "df.w", "er45", "kwer", "KJWE", "RWWE")]
        [TestCase(_MEDIUM_NOT_EVEN_VALUE, 20, "aB@sdf.wer45kwerKJWE")]
        [TestCase(_MEDIUM_EVEN_VALUE, 4, "[1LD", "Nj,e", "25nw", ",sj3", "504k")]
        [TestCase(_MEDIUM_EVEN_VALUE, 10, "[1LDNj,e25", "nw,sj3504k")]
        [TestCase(_MEDIUM_EVEN_VALUE, 3, "[1L", "DNj", ",e2", "5nw", ",sj", "350")]
        [TestCase(_LARGE_VALUE, 77, "SDF25343123asdfwsf2e@seflmwer.,wer[posdfqlkwjepoqwe3213qwe24126354123123qADSs")]
        public void SplitToLexems_SingleLessLength_ValidResult(string value, int length, params string[] lexems)
        {
            TestSplitToLexems(value, new[] {length}, lexems);
        }

        /// <summary>
        /// Тестируемый метод: SplitToLexems
        /// Сценарий: Передаем несколько размеров лексем, которые больше длины строки
        /// Ожидаемый результат: Метод не вернул лексем
        /// </summary>        
        [TestCase(_SMALL_VALUE, new int[] { 4})]
        [TestCase(_SMALL_VALUE, new int[] { 4, 100, 200 })]
        [TestCase(_MEDIUM_NOT_EVEN_VALUE, new int[] { 999})]
        [TestCase(_MEDIUM_NOT_EVEN_VALUE, new int[] { 30, 999, 1000 })]
        [TestCase(_MEDIUM_NOT_EVEN_VALUE, new int[] { 1000, 30, 101 })]
        [TestCase(_MEDIUM_EVEN_VALUE, new int[] { 120 })]
        [TestCase(_MEDIUM_EVEN_VALUE, new int[] { 120, 201, 200 })]
        public void SplitToLexems_OnlyLargerLengths_NoLexems(string value, int[] length)
        {
            TestSplitToLexems(value, length);
        }

        /// <summary>
        /// Тестируемый метод: SplitToLexems
        /// Сценарий: Передаем несколько размеров лексем, где только один размер меньше или равен длине разбиваемой строки
        /// Ожидаемый результат: Метод вернул корректные лексемы
        /// </summary>
        [TestCase(_SMALL_VALUE, new int[]{ 4, 3, 200 }, _SMALL_VALUE)]
        [TestCase(_SMALL_VALUE, new int[] { 3, 4 }, _SMALL_VALUE)]
        [TestCase(_SMALL_VALUE, new int[] { 4, 5,1000,3 }, _SMALL_VALUE)]
        [TestCase(_SMALL_VALUE, new int[] { 10, 2, 200 }, "aB")]
        [TestCase(_LARGE_VALUE, new int[]{ 79, 78, 100} , _LARGE_VALUE)]
        [TestCase(_LARGE_VALUE, new int[] { 79, 77, 100 },
            "SDF25343123asdfwsf2e@seflmwer.,wer[posdfqlkwjepoqwe3213qwe24126354123123qADSs")]      
        public void SplitToLexems_OnlyOneLessOrEqualLength_ValidResult(string value, int[] lengths, params string[] lexems)
        {
            TestSplitToLexems(value, lengths, lexems);
        }

        /// <summary>
        /// Тестируемый метод: SplitToLexems
        /// Сценарий: Передаем несколько размеров лексем в перемешку большие или нет
        /// Ожидаемый результат: Метод корректно отработал и вернул правильные данные
        /// </summary>
        /// <param name="value">Исходное значение</param>
        /// <param name="lengths">Массив длинн лексем</param>
        /// <param name="lexems">Какие на выходы должны получить лексемы</param>
        [TestCase(_SMALL_VALUE, new int[] {2, 1}, "aB", "c")]
        [TestCase(_MEDIUM_NOT_EVEN_VALUE, new int[] {8, 4, 3, 2, 1}, "aB@sdf.w", "er45kwer", "KJWERWWE", "R")]
        [TestCase("aB@sdf.wer45kwerKJWERWWERe", new int[] {8, 4, 3, 2, 1}, "aB@sdf.w", "er45kwer", "KJWERWWE", "Re")]
        [TestCase("aB@sdf.wer45kwerKJWERWWEReer", new int[] {8, 4, 3, 2, 1}, "aB@sdf.w", "er45kwer", "KJWERWWE", "Reer")]
        [TestCase(_LARGE_VALUE, new int[] {70, 80, 7, 1},
            "SDF25343123asdfwsf2e@seflmwer.,wer[posdfqlkwjepoqwe3213qwe241263541231", "23qADSs", "a")]
        public void SplitToLexems_ComplexTest_ValidResult(string value, int[] lengths, params string[] lexems)
        {
            TestSplitToLexems(value, lengths, lexems);
        }

        /// <summary>
        /// Тестируемый метод: SplitToLexems
        /// Сценарий: Проверяем, что в переданном порядке разбивается строка на лексемы
        /// Ожидаемый результат: Метод корректно отработал и вернул правильные данные
        /// </summary>
        [TestCase(_SMALL_VALUE, new int[]{1, 2}, "a","B","c")]
        [TestCase("1234567890", new int[] { 7, 1, 3 }, "1234567", "8", "9", "0")]
        public void SplitToLexems_CheckOrder(string value, int[] lengths, params string[] lexems)
        {
            TestSplitToLexems(value, lengths, lexems);
        }




        /// <summary>
        /// Вызывает проверку метода SplitToLexems
        /// </summary>
        /// <param name="value">Исходное значение</param>
        /// <param name="lengths">Массив длинн лексем</param>
        /// <param name="lexems">Какие на выходы должны получить лексемы</param>
        private void TestSplitToLexems(string value, int[] lengths, params string[] lexems)
        {
            var utility = new LexemUtility();
            var result = utility.SplitToLexems(value, lengths).ToArray();
            Assert.AreEqual(lexems.Length, result.Length);

            for (int i = 0; i < result.Length; i++)
            {
                Assert.AreEqual(lexems[i], result[i]);
            }
        }
    }
}
