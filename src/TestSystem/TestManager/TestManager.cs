﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using TestSystem.Common;

namespace TestSystem.TestManager
{
    class TestManager
    {
        /// <summary>
        /// Сколько пользователей загружать в менеджер при старте
        /// </summary>
        public int UserCount { get; set; } = 500000;

        /// <summary>
        /// Сколько пользователей искать в тесте 1
        /// </summary>
        public int UserCountForTest1 { get; set; } = 250;

        /// <summary>
        /// Подстрок длины 1-3 символа в тесте 2 (минимум 326, максимум  17902)
        /// </summary>
        public int SubstringCountForTest2 { get; set; } = 2500;

        /// <summary>
        /// Разрешены ли пропуски id - в последовательности
        /// </summary>
        public bool AllowSkipResults { get; set; } = false;

        /// <summary>
        /// Только итоговый тест, без разбивки
        /// </summary>
        public bool OnlyComplexTest { get; set; } = false;

        private List<User> _users;

        public void Init()
        {
            var users = new List<User>();
            Console.WriteLine("Загрузка данных...");
            var sw = Stopwatch.StartNew();
            var lines = File.ReadAllLines(@"data\emails1kk.txt");
            foreach (var line in lines)
            {
                var userLine = line.Split(';');
                var user = new User
                {
                    Id = int.Parse(userLine[0]),
                    Email = userLine[1],
                    Login = userLine[2],
                    Phone = userLine[3]
                };
                users.Add(user);
            }
            _users = users;
            sw.Stop();
            Console.WriteLine($"Загрузка данных завершена: пользователей = {lines.Length}, время = {sw.Elapsed}");
        }

        private static void ShuffleList<T>(List<T> list)
        {
            var rnd = new Random(DateTime.UtcNow.Millisecond);
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rnd.Next(n + 1);
                var value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static decimal TruncateDecimal(decimal value, int decimalPlaces)
        {
            decimal integralValue = Math.Truncate(value);
            decimal fraction = value - integralValue;
            decimal factor = (decimal)Math.Pow(10, decimalPlaces);
            decimal truncatedFraction = Math.Truncate(fraction * factor) / factor;
            decimal result = integralValue + truncatedFraction;
            return result;
        }

        private static IUserManager FillManager(IUserManager manager, List<User> users)
        {            
            Console.WriteLine($"[{manager.Name}]: Инициализация менеджера...");
            long notifyCnt = users.Count / 10;
            long k = 0;
            var sw = Stopwatch.StartNew();
            try
            {
                var sw2 = Stopwatch.StartNew();
                for (int i = 0; i < users.Count; i++)                
                {
                    manager.Add(users[i]);
                    k++;
                    if (i % notifyCnt == 0)
                    {
                        Console.WriteLine($"[{manager.Name}]: {i}: {sw.Elapsed} ({sw2.Elapsed})");
                        sw2.Restart();
                    }
                }
                Console.WriteLine($"[{manager.Name}]: load = {k}/{users.Count} ({sw.Elapsed})");
                //((SimpleUserManager) manager).Check();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            Console.WriteLine();
            return manager;
        }

        /// <summary>
        /// Тест на полное совпадение по строке. 
        /// В запросах есть как существующие записи так и не существующие.
        /// </summary>   
        /// <returns></returns>
        private static List<string> BuildTest1Queries(List<User> users, int count)
        {
            var rnd = new Random(DateTime.UtcNow.Millisecond);
            for (int i = 0; i < 100; i++)
                rnd.Next(250000);
            var shift = rnd.Next(250000);
            var source = users.Skip(shift).Take(count).Concat(users.Skip(600000 + shift).Take(count)).ToList();
            var queries = new List<string>();
            foreach (var user in source)
            {
                queries.Add(user.Email);
                queries.Add(user.Login);
                if (!string.IsNullOrEmpty(user.Phone))
                    queries.Add(user.Phone);
            }
            ShuffleList(queries);
            return queries;
        }

        /// <summary>
        /// Тест на основе подстрок вида a..z, aa..zz, aaa..zzz, ...
        /// </summary>        
        /// <returns></returns>
        private static List<string> BuildTest2Queries(int count)
        {
            var queries = new List<string>[3];
            for (int length = 1; length < 4; length++)
            {
                queries[length - 1] = new List<string>();
                var s = new Substring(length);
                queries[length - 1].Add(s.Current);
                while (s.GetNext())
                {
                    queries[length - 1].Add(s.Current);
                }
            }

            ShuffleList(queries[0]);
            ShuffleList(queries[1]);
            ShuffleList(queries[2]);

            var result = queries[0];
            result.AddRange(queries[1].Take(300));
            result.AddRange(queries[2].Take(count - result.Count));

            return result;
        }

        private static (long, TimeSpan) TestForQueries(IUserManager manager, string testName, List<string> queries, int limit)
        {
            long cnt = queries.Count;
            decimal dCnt = cnt;
            Console.Write($"[{manager.Name}]: {testName} [запросов={cnt}, limit = {limit}]...  ");
            int cursorPos = Console.CursorLeft;
            decimal lastP = 0m;
            long k = 0;
            var sw = Stopwatch.StartNew();
            foreach (var query in queries)
            {
                k++;
                var results = manager.Find(query, limit);
                decimal currP = k / dCnt;
                if (currP >= lastP + 0.01m)
                {
                    sw.Stop();
                    lastP = currP;
                    ConsoleHelper.PrintPercent(cursorPos, TruncateDecimal(currP * 100, 0));
                    //Console.Write($"{TruncateDecimal(currP*100, 0)}% ");
                    sw.Start();
                }
            }
            sw.Stop();
            ConsoleHelper.PrintPercent(cursorPos, TruncateDecimal(100, 0));
            Console.WriteLine();
            Console.WriteLine($"[{manager.Name}]: {testName} завершен, время = {sw.Elapsed}");
            Console.WriteLine();
            return (queries.Count, sw.Elapsed);
        }

        private static decimal CompateTestResults(IUserManager baseManager, (long, TimeSpan) baseResults, IUserManager targetManager, (long, TimeSpan) targerResults)
        {
            Console.WriteLine($"Сравнение результатов:");
            Console.WriteLine($"[{baseManager.Name}] запросов: {baseResults.Item1}, время теста: {baseResults.Item2}, время на запрос: {baseResults.Item2.TotalSeconds / baseResults.Item1}s");
            Console.WriteLine($"[{targetManager.Name}] запросов: {targerResults.Item1}, время теста: {targerResults.Item2}, время на запрос: {targerResults.Item2.TotalSeconds / targerResults.Item1}s");
            decimal score = (decimal)(baseResults.Item2.TotalSeconds / targerResults.Item2.TotalSeconds);
            Console.WriteLine($"Score: {TruncateDecimal(score, 3)}");
            Console.WriteLine("------");
            Console.WriteLine("");
            return score;
        }

        /// <summary>
        /// Проверяем удовлетворяет ли пользователь указанной строке поиска 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="query"></param>
        /// <returns></returns>        
        public static bool CheckQuery(User user, string query)
        {
            return user.Login.ToLower().Contains(query) ||
                   user.Email.ToLower().Contains(query) ||
                   user.Phone.ToLower().Contains(query);
        }

        /// <summary>
        /// Проверяет результаты работы менеджеров на корректность
        /// </summary>
        /// <param name="baseManager">эталонный менеджер</param>
        /// <param name="targetManager">тестируемый</param>
        /// <param name="queries">список запросов для теста</param>
        /// <param name="allowSkip">разрешены ли пропуски</param>
        private static bool VerifyResults(IUserManager baseManager, IUserManager targetManager, List<string> queries, bool allowSkip)
        {
            var limits = new List<int> { 1, 10, 100, 500 };
            long qCount = queries.Count * 4;
            decimal dCnt = qCount;
            Console.WriteLine($"Прогрев и проверка корректности работы (запросов={qCount}, allowSkip={allowSkip})...");
            Console.Write("Прогресс: ");
            int cursorPos = Console.CursorLeft;
            long cntErrors = 0;
            decimal lastP = 0m;
            long k = 0;
            var sw = new Stopwatch();
            foreach (var query in queries)
            {
                foreach (var limit in limits)
                {
                    var bRes = baseManager.Find(query, limit);
                    var tRes = targetManager.Find(query, limit);
                    if (limit > 1 && bRes.Count == limit)
                    {
                        ;
                    }
                    bool stop = false;
                    if (bRes.Count != tRes.Count) // Кол-во всегда должно совпадать
                    {
                        cntErrors++;
                        stop = true;
                    }

                    if (!stop)
                    {
                        if (!allowSkip) // Если не разрешены пропуски результаты должны полностью совпадать.
                        {
                            bool equals = true;
                            for (int i = 0; i < bRes.Count; i++)
                            {
                                equals = bRes[i].Id == tRes[i].Id;
                                if (!equals)
                                    break;
                            }
                            if (!equals)
                                cntErrors++;
                        }
                        else // Возможны пробелы, только проверяем корректность
                        {
                            int lastId = 0;
                            foreach (var user in tRes) // Проверим возрастающая ли последовательность и отсуствие дублей
                            {
                                if (user.Id <= lastId)
                                {
                                    cntErrors++;
                                    break;
                                }
                                lastId = user.Id;
                            }
                            if (!tRes.All(e => CheckQuery(e, query.ToLower())))
                                cntErrors++;
                        }
                    }

                    k++;
                    decimal currP = k / dCnt;
                    if (currP >= lastP + 0.01m)
                    {

                        lastP = currP;
                        ConsoleHelper.PrintPercent(cursorPos, TruncateDecimal(currP * 100, 0));
                    }
                }
            }
            sw.Stop();
            ConsoleHelper.PrintPercent(cursorPos, TruncateDecimal(100, 0));
            Console.WriteLine();
            Console.WriteLine($"Проверка завершена: time = {sw.Elapsed}, errors = {cntErrors}, errors = {TruncateDecimal((decimal)cntErrors / qCount * 100, 2)}%");
            bool success = cntErrors == 0;
            string resultStr = success ? "Успех!" : "Провален!";
            Console.WriteLine($"Результат: {resultStr}");
            Console.WriteLine();
            return success;
        }

        /// <summary>
        /// Тесты с различными типами запросов чтобы смотреть где проседает
        /// </summary>
        private void SplitTests(IUserManager baseManager, IUserManager targetManager)
        {
            var scores = new List<decimal>();

            string test1name = "Тест1 (Полное совпадение)";

            var queriesForTest1 = BuildTest1Queries(_users, UserCountForTest1);
            scores.Add(CompateTestResults(baseManager, TestForQueries(baseManager, test1name, queriesForTest1, 1),
                targetManager, TestForQueries(targetManager, test1name, queriesForTest1, 1)));

            queriesForTest1 = BuildTest1Queries(_users, UserCountForTest1);
            scores.Add(CompateTestResults(baseManager, TestForQueries(baseManager, test1name, queriesForTest1, 10),
                targetManager, TestForQueries(targetManager, test1name, queriesForTest1, 10)));

            queriesForTest1 = BuildTest1Queries(_users, UserCountForTest1);
            scores.Add(CompateTestResults(baseManager, TestForQueries(baseManager, test1name, queriesForTest1, 100),
                targetManager, TestForQueries(targetManager, test1name, queriesForTest1, 100)));

            queriesForTest1 = BuildTest1Queries(_users, UserCountForTest1);
            scores.Add(CompateTestResults(baseManager, TestForQueries(baseManager, test1name, queriesForTest1, 500),
                targetManager, TestForQueries(targetManager, test1name, queriesForTest1, 500)));


            string test2name = "Тест2 (Подстроки)";

            var queriesForTest2 = BuildTest2Queries(SubstringCountForTest2);
            scores.Add(CompateTestResults(baseManager, TestForQueries(baseManager, test2name, queriesForTest2, 1),
                    targetManager, TestForQueries(targetManager, test2name, queriesForTest2, 1)));

            queriesForTest2 = BuildTest2Queries(SubstringCountForTest2);
            scores.Add(CompateTestResults(baseManager, TestForQueries(baseManager, test2name, queriesForTest2, 10),
                targetManager, TestForQueries(targetManager, test2name, queriesForTest2, 10)));

            queriesForTest2 = BuildTest2Queries(SubstringCountForTest2);
            scores.Add(CompateTestResults(baseManager, TestForQueries(baseManager, test2name, queriesForTest2, 100),
                targetManager, TestForQueries(targetManager, test2name, queriesForTest2, 100)));

            queriesForTest2 = BuildTest2Queries(SubstringCountForTest2);
            scores.Add(CompateTestResults(baseManager, TestForQueries(baseManager, test2name, queriesForTest2, 500),
                targetManager, TestForQueries(targetManager, test2name, queriesForTest2, 500)));


            var score = scores.Sum() / scores.Count;
            var sb = new StringBuilder();
            foreach (var scoreItem in scores)
                sb.Append($"{TruncateDecimal(scoreItem, 3)}/");
            sb.Remove(sb.Length - 1, 1);

            Console.WriteLine($"Total Scores: {sb}");
            Console.WriteLine();
            Console.WriteLine();
        }

        public void Compare(IUserManager baseManager, IUserManager targetManager)
        {
            var users = _users.Take(UserCount*4).ToList();
            ShuffleList(users);
            users = users.Take(UserCount).ToList();            
            FillManager(baseManager, users);
            FillManager(targetManager, users);

            // Тест корректности и прогрев 
            var q1 = BuildTest1Queries(_users, 20);
            q1.AddRange(BuildTest2Queries(1));            
            ShuffleList(q1);
            q1.Insert(0, "");
            if (!VerifyResults(baseManager, targetManager, q1, AllowSkipResults))
                return;

            if (!OnlyComplexTest)
            {
                SplitTests(baseManager, targetManager);
            }

            string testName = "ComplexText";
            var qr = new List<string>();
            qr.AddRange(BuildTest1Queries(_users, UserCountForTest1 * 2));
            qr.AddRange(BuildTest2Queries(SubstringCountForTest2 * 2));
            ShuffleList(qr);
            var totalScore = CompateTestResults(baseManager, TestForQueries(baseManager, testName, qr, 100),
                targetManager, TestForQueries(targetManager, testName, qr, 100));

            Console.WriteLine($"Total Score: {TruncateDecimal(totalScore, 3)}");

            Console.WriteLine("-------------");
        }

        public void RunUserTest(IUserManager baseManager, IUserManager targetManager)
        {
            Console.WriteLine("Ручное сравнение:");
            while (true)
            {
                string s = Console.ReadLine();
                var s1 = Stopwatch.StartNew();
                var us1 = baseManager.Find(s, 10);
                s1.Stop();
                var s2 = Stopwatch.StartNew();
                var us2 = targetManager.Find(s, 10);
                s2.Stop();

                Console.Write($"[{baseManager.Name}] {s1.Elapsed}: ");
                foreach (var u in us1)
                    Console.Write($"{u.Id}, ");
                Console.WriteLine();
                Console.Write($"[{targetManager.Name}] {s2.Elapsed}: ");
                foreach (var u in us2)
                    Console.Write($"{u.Id}, ");
                Console.WriteLine();
            }

        }

        public void SetBenchmarkParams()
        {
            OnlyComplexTest = true;
            AllowSkipResults = false; 
            UserCount = 500000; 
            UserCountForTest1 = 250; 
            SubstringCountForTest2 = 2500;
        }
    }
}
