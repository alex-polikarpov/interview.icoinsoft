﻿using System;
using TestSystem.CandidatSolve.SplitLexems;
using TestSystem.SimpleSolve;

namespace TestSystem
{
    class Program
    {
        static void Main(string[] args)
        {
            GC.TryStartNoGCRegion(250000000);

            Console.WriteLine("Press any key for start...");
            //Console.ReadKey();            

            var test = new TestManager.TestManager();

            // -- Настройки для бенчмарка (при разработке для удобства можно менять)            

            //test.AllowSkipResults = true; // При проверке = false
            test.UserCount = 500000; // при проверке = 500000
            test.UserCountForTest1 = 250; // при проверке = 250
            test.SubstringCountForTest2 = 2500; // при проверке = 2500
            //test.OnlyComplexTest = true;
           
            //test.SetBenchmarkParams();
            // ----

            test.Init();

            var bManager = new SimpleUserManager(); 
             var tManager = new SplitLexemsRepositoryFromPolikarpov(); // Заменить на вашу реализацию
            
            test.Compare(bManager, tManager); 

            test.RunUserTest(bManager, tManager);

            Console.ReadKey();
        }
    }
}
