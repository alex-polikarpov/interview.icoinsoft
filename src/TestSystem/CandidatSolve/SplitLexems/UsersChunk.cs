﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using TestSystem.CandidatSolve.Base;
using TestSystem.Common;

namespace TestSystem.CandidatSolve.SplitLexems
{
    /// <summary>
    /// Набор пользователей
    /// </summary>
    public class UsersChunk
    {
        private const int _INCORRECT_ID = -1;

        public UsersChunk(int? capacity = null)
        {
            MaxId = _INCORRECT_ID;
            if (capacity.HasValue)
            {
                UserWrappers = new SortedList<int, SimpleUserSearchInfo>(capacity.Value);
            }
            else UserWrappers = new SortedList<int, SimpleUserSearchInfo>();
        }

        /// <summary>
        /// Количество пользователей в чанке
        /// </summary>
        public int Count
        {
            get { return UserWrappers.Count; }
        }

        /// <summary>
        /// Максимальный Id пользователя в чанке. -1 если нет пользователей
        /// </summary>
        public int MaxId { get; private set; }

        /// <summary>
        /// Данные чанка
        /// </summary>
        public SortedList<int, SimpleUserSearchInfo> UserWrappers { get; private set; }
        


        /// <summary>
        /// Пытается добавить пользователя в чанк. Если набор уже заполнен и Id
        /// добавляемого пользователя меньше, чем MaxId, то удаляет юзера с MaxId и добавляет нового
        /// </summary>
        public void AddOrReplace(SimpleUserSearchInfo user)
        {
            if (UserWrappers.ContainsKey(user.GetId()))
                return;

            if (UserWrappers.Count < UserConstants.LIMIT_MAX)
            {
                AddUser(user);
                return;
            }

            if (user.GetId() > MaxId)
                return;

            var values = UserWrappers.Values;
            var last = values[values.Count - 1];
            MaxId = values[values.Count - 2].GetId();
            UserWrappers.Remove(last.GetId());
            AddUser(user);
        }

        /// <summary>
        /// Удаялет пользователя из набора данных
        /// </summary>
        public void Remove(SimpleUserSearchInfo user)
        {
            if(!UserWrappers.ContainsKey(user.GetId()))
                return;

            UserWrappers.Remove(user.GetId());

            if(UserWrappers.Count == 0)
            {
                MaxId = _INCORRECT_ID;
                return;
            }

            var values = UserWrappers.Values;
            var last = values[values.Count - 1];
            MaxId = last.GetId();
        }

        public List<User> GetUsers(int maxCount)
        {
            return UserWrappers.Values.Take(maxCount).Select(x=>x.User).ToList();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void AddUser(SimpleUserSearchInfo user)
        {            
            var userId = user.GetId();
            UserWrappers.Add(userId, user);

            if (userId > MaxId)
                MaxId = userId;
        }
    }
}
