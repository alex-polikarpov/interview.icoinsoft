﻿using TestSystem.Common;

namespace TestSystem.CandidatSolve.Base
{
    public abstract class BaseUserWrapper
    {
        public BaseUserWrapper(User user)
        {
            User = user;
            Update(user);
        }

        public User User { get; private set; }

        public abstract void Update(User user);
    }
}
