﻿using System;
using System.Collections.Generic;
using TestSystem.CandidatSolve.Extensions;
using TestSystem.Common;

namespace TestSystem.CandidatSolve.Base
{
    public abstract class BaseUserRepository<TUserWrapper> : IUserManager
        where TUserWrapper : BaseUserWrapper
    {
        protected readonly SortedList<int, TUserWrapper> _users = new SortedList<int, TUserWrapper>(UserConstants.MAX_USERS_COUNT);
        protected readonly object _lock = new object();

        public abstract string Name { get; }

        public virtual void Add(User user)
        {
            user.Validate();

            lock (_lock)
            {
                if (_users.ContainsKey(user.Id))
                    throw new ArgumentException(string.Format(UserConstants.Errors.USER_ALREADY_ADDED, user.Id));

                if (_users.Count == UserConstants.MAX_USERS_COUNT)
                    throw new InvalidOperationException(UserConstants.Errors.MAXUMUM_USERS_ADDED);

                _users.Add(user.Id, CreateUserWrapper(user));
            }
        }

        public virtual List<User> Find(string query, int maxCount)
        {
            if (query == null || query.Length > UserConstants.EMAIL_MAX_LENGTH ||
                maxCount < 1 || maxCount > UserConstants.LIMIT_MAX)
            {
                return new List<User>();
            }

            if (query.Contains(UserConstants.WHITE_SPACE))
                throw new ArgumentException(UserConstants.Errors.QUERY_CANT_CONTAINS_WHITE_SPACES);

            lock (_lock)
            {
                if (query == string.Empty)
                {
                    var result = new List<User>(maxCount);

                    foreach (var userInfo in _users.Values)
                    {
                        result.Add(userInfo.User);

                        if (result.Count == maxCount)
                            return result;
                    }
                }

                return FindUsers(query, maxCount);
            }
        }

        public virtual void Update(User user)
        {
            user.Validate();

            lock (_lock)
            {
                if (!_users.ContainsKey(user.Id))
                    return;

                var userWrapper = _users[user.Id];
                userWrapper.Update(user);
                OnUpdate(userWrapper);
            }
        }


        /// <summary>
        /// Метод, создающий обертку над <see cref="User"/>
        /// </summary>
        protected abstract TUserWrapper CreateUserWrapper(User user);

        /// <summary>
        /// Метод, который ищет пользователей. Вызывается после проверок query
        /// </summary>
        protected abstract List<User> FindUsers(string query, int maxCount);

        protected virtual void OnUpdate(TUserWrapper user)
        {

        }
    }
}
