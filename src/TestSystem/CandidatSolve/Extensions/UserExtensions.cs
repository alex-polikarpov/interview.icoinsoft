﻿using System;
using TestSystem.Common;

namespace TestSystem.CandidatSolve.Extensions
{
    public static class UserExtensions
    {
        private const string _INCORRECT_VALUE_TEMPLATE = "Incorrect {0} value";
        private const string _WHITE_SPACE_ERROR = "Values can't contains white spaces";

        public static void Validate(this User user)
        {
            if(user == null)
                throw new ArgumentNullException("User is null");

            if (user.Id < 1)
                throw new ArgumentException(string.Format(_INCORRECT_VALUE_TEMPLATE, nameof(user.Id)));

            if (string.IsNullOrEmpty(user.Email))
                throw new ArgumentException(string.Format(_INCORRECT_VALUE_TEMPLATE, nameof(user.Email)));

            if (user.Email.Length < UserConstants.EMAIL_MIN_LENGTH ||
                user.Email.Length > UserConstants.EMAIL_MAX_LENGTH)
                throw new ArgumentException(string.Format(_INCORRECT_VALUE_TEMPLATE, nameof(user.Email)));

            if (user.Email.Contains(UserConstants.WHITE_SPACE))
                throw new ArgumentException(_WHITE_SPACE_ERROR);

            if (user.Login != null)
            {
                if (user.Login.Length > UserConstants.LOGIN_MAX_LENGTH)
                    throw new ArgumentException(string.Format(_INCORRECT_VALUE_TEMPLATE, nameof(user.Login)));

                if (user.Login.Contains(UserConstants.WHITE_SPACE))
                    throw new ArgumentException(_WHITE_SPACE_ERROR);
            }

            if (user.Phone != null)
            {
                if (user.Phone.Length > UserConstants.PHONE_MAX_LENGTH)
                    throw new ArgumentException(string.Format(_INCORRECT_VALUE_TEMPLATE, nameof(user.Phone)));

                if (user.Phone.Contains(UserConstants.WHITE_SPACE))
                    throw new ArgumentException(_WHITE_SPACE_ERROR);
            }
        }
    }
}
